# frozen_string_literal: true

require 'require_all'
require 'rest-client'
require 'json'
require_rel 'warframe'

# Warframe Stat Ruby Client main module
module Warframe; end
