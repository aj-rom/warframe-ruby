# frozen_string_literal: true

module Warframe
  # The current version of this gem.
  VERSION = '1.0.0'
end
