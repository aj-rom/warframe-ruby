# frozen_string_literal: true

module Warframe::Models::Attributes
  # Adds the eta attribute.
  module ETA
    # The estimated time until arrival.
    # @return [String]
    attr_reader :eta
  end
end
