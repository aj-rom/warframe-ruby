# frozen_string_literal: true

# The types of rewards you can get from this event.
module Warframe::Models::Attributes
  module RewardTypes
    # The types of rewards you can get from this event.
    # @return [Array<String>]
    attr_reader :reward_types
  end
end
