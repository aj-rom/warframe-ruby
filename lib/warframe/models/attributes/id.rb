# frozen_string_literal: true

# Unique data response ID.
module Warframe::Models::Attributes
  module ID
    # The unique ID of the event.
    # @return [String]
    attr_reader :id
  end
end
