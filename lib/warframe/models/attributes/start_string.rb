# frozen_string_literal: true

# Adds the start_string attribute, for when an event will begin.
module Warframe::Models::Attributes
  module StartString
    # When this event goes into effect.
    # @return [String]
    attr_reader :start_string
  end
end
