# frozen_string_literal: true

# Includes the translations attribute.
module Warframe::Models::Attributes
  module Translations
    # The message that can be translated.
    # @return [String]
    attr_reader :message

    # Available translations for the requested data.
    # @return [OpenStruct]
    attr_reader :translations
  end
end
