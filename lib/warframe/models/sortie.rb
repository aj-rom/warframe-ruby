# frozen_string_literal: true

require_rel 'base'

# Sortie data model.
# {https://api.warframestat.us/pc/sortie /:platform/sortie}
module Warframe::Models
  class Sortie < Base
    include Attributes::ID
    include Attributes::ActiveBoth
    include Attributes::Expiration
    include Attributes::ETA

    # The boss for this part of the sortie.
    # @return [String]
    attr_reader :boss

    # The faction fighting you in this mission.
    # @return [String]
    attr_reader :faction

    # Modifiers active for this challenge.
    # @return [Array<OpenStruct>]
    attr_reader :variants

    # The reward pool which this is pulling from.
    # @return [String]
    attr_reader :reward_pool
  end
end
