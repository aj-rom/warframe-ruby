# frozen_string_literal: true

require_rel 'base'

# SyndicateMission data model.
# {https://api.warframestat.us/pc/syndicateMissions /:platform/syndicateMissions}
module Warframe::Models
  class SyndicateMission < Base
    include Attributes::ETA
    include Attributes::ID
    include Attributes::ActiveBoth
    include Attributes::StartString

    # 'Jobs' or challenges currently available.
    # @return [Array<OpenStruct>]
    attr_reader :jobs

    # The syndicate you will be fighting.
    # @return [String]
    attr_reader :syndicate

    # The nodes that this mission is available on.
    # @return [Array]
    attr_reader :nodes
  end
end
