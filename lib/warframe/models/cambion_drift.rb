# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Cambion Drift data model.
  # {https://api.warframestat.us/pc/cambionCycle /:platform/cambionCycle}
  class CambionDrift < Base
    include Attributes::ID
    include Attributes::Activation
    include Attributes::Expiry

    # Current active state of the world, either 'vome' or 'fass'.
    # @return [String]
    attr_reader :active

    # The time remaining until world state switches.
    # @return [String]
    attr_reader :time_left
  end
end
