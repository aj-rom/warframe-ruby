# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Model for VallisCycle data.
  # {https://api.warframestat.us/pc/vallisCycle /:platform/vallisCycle}
  class VallisCycle < Base
    include Attributes::ID
    include Attributes::Expiry
    include Attributes::Activation

    # @!attribute time_left
    # @return [String] time left until next cycle.
    attr_reader :time_left

    # @!attribute is_warm?
    # @return [Boolean] whether or not it currently is warm.
    attr_reader :is_warm
    alias is_warm? is_warm

    # @!attribute state
    # @return [String] the current world state (Cold or Warm)
    attr_reader :state

    # @!attribute short_string
    # @return [String] the time remaining until state change. Ex: `5m to Warm`.
    attr_reader :short_string
  end
end
