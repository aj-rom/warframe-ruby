# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Cetus data model.
  # {https://api.warframestat.us/pc/cetusCycle /:platform/cetusCycle}
  class Cetus < Base
    include Attributes::ID
    include Attributes::Activation
    include Attributes::Expiry

    # Whether or not it is currently day.
    # @return [Boolean]
    attr_reader :is_day
    alias day? is_day

    # Current world state of Cetus.
    # @return [String]
    attr_reader :state

    # Whether or not this is Cetus.
    # @return [Boolean]
    attr_reader :is_cetus
    alias cetus? is_cetus

    # Time left until state change.
    # @return [String]
    attr_reader :time_left

    # A short string of the time left until state change.
    # @return [String]
    attr_reader :short_string
  end
end
