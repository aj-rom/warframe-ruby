# frozen_string_literal: true

require 'fast_underscore'
require_rel 'attributes'
require_rel 'objectified_hash'

module Warframe::Models
  # Warframe Base Model.
  class Base < ObjectifiedHash
  end
end
