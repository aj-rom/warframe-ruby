# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Nightwave data model.
  # {https://api.warframestat.us/pc/nightwave /:platform/nightwave}
  class Nightwave < Base
    include Attributes::Activation
    include Attributes::ID
    include Attributes::Expiry
    include Attributes::RewardTypes

    # The current phase of this event.
    # @return [Integer]
    attr_reader :phase

    # The current Nightwave Tag
    # @return [String]
    attr_reader :tag

    # The current Nightwave Season
    # @return [Integer]
    attr_reader :season

    # List of all possible challenges.
    # @return [Array<OpenStruct>]
    attr_reader :possible_challenges

    # Active Challenges for this event.
    # @return [Array<OpenStruct>]
    attr_reader :active_challenges
  end
end
