# frozen_string_literal: true

module Warframe::Models
  # An objectified hash response. Used for turning raw JSON objects into a ruby friendly object.
  # This can be accessed in multiple formats, ie: JSON `obj['String']` access, Ruby hash `obj[:symbol]`,
  # Ruby object `obj.method`.
  # This also handles serialization of nested hashes or arrays of hashes, which in turn will become
  # a Waframe::Models::ObjectifiedHash.
  class ObjectifiedHash
    def initialize(data)
      @data = transform_to_hash(data)
      populate_object
    end

    def [](key)
      @data[key] || @data[string_to_symbol(key)]
    end

    def to_hash # rubocop:todo Metrics/CyclomaticComplexity
      @to_hash ||= @data.each_with_object({}) do |(k, v), hash|
        v = v.to_h if v.is_a?(Warframe::Models::ObjectifiedHash) && !v.empty?
        v = v.map(&:to_h) if v.is_a?(Array) && v.first.is_a?(Warframe::Models::ObjectifiedHash)
        hash[k] = v
      end
    end
    alias to_h to_hash
    alias hash to_hash

    def to_s
      @to_s ||= hash.to_s
    end

    def self.from_array(arr)
      arr.map { |data| new(data) if data.is_a?(Hash) }.compact
    end

    # @return [String] Formatted string with the class name, object id and original hash.
    def inspect
      "#<#{self.class}:#{object_id} { data: #{data.inspect} }".gsub('\"', '')
    end

    def size
      @data.size
    end

    private

    attr_reader :data

    def populate_object # rubocop:todo Metrics/AbcSize
      data.each do |k, v|
        if v.is_a?(Hash) && v.size.positive?
          v = ObjectifiedHash.new(v)
        elsif v.is_a?(Array) && v.first.is_a?(Hash) && v.first.size.positive?
          v = ObjectifiedHash.from_array(v)
        end

        self.class.send(:attr_reader, k)
        instance_variable_set("@#{k}", v)
        @data[k] = v
      end
    end

    def transform_to_hash(hash)
      hash.transform_keys { |data| string_to_symbol(data) }
    rescue NoMethodError
      pp "Error decoding: #{hash}"
    end

    def string_to_symbol(string)
      string.split(/(?=[A-Z])/).map(&:downcase).join('_').to_sym
    end
  end
end
