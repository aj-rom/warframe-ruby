# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Global Upgrades data model.
  # {https://api.warframestat.us/pc/globalUpgrades /:platform/globalUpgrades}
  class GlobalUpgrade < Base
    include Attributes::Expired
    include Attributes::Desc
    include Attributes::ETA

    # The start of the global upgrade.
    # @return [String]
    attr_reader :start

    # The end of the global upgrade.
    # @return [String]
    attr_reader :end

    # The upgrade to be received.
    # @return [String]
    attr_reader :upgrade

    # The operation of the global upgrade.
    # @return [String]
    attr_reader :operation

    # The symbol of the operation.
    # @return [String]
    attr_reader :operation_symbol

    # The upgrade operation value.
    # @return [Integer]
    attr_reader :upgrade_operation_value
  end
end
