# frozen_string_literal: true

require_rel 'base'

module Warframe::Models
  # Model for Alert data.
  # {https://api.warframestat.hub/pc/alerts /:platform/alerts}
  class Alert < Base
    include Attributes::Expired
    include Attributes::Active
    include Attributes::ETA
    include Attributes::RewardTypes
    include Attributes::StartString

    # The id of the Alert.
    # @return [String]
    attr_reader :id

    # The mission data of this alert.
    # @return [OpenStruct]
    attr_reader :mission
  end
end
