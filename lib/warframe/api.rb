# frozen_string_literal: true

require_rel 'api'
require_rel 'models'

# The API Router for getting live data.
#
# Attempting to have every accessible route from {https://docs.warframestat.us Warframe Stat}.
#
# Module names are 'routes' to this API. See {Warframe::API::Alerts Alerts} for example.
module Warframe::API
  include Alerts
  include CambionDrift
  include Cetus
  include ConclaveChallenges
  include GlobalUpgrades
  include Invasions
  include News
  include Nightwave
  include Sortie
  include SteelPath
  include SyndicateMissions
  include VallisCycle
end
