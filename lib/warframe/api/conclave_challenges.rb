# frozen_string_literal: true

require_rel '../models/conclave_challenge'

module Warframe::API
  # API endpoint for getting information on current Conclave Challenge data.
  #
  # {https://api.warframestat.us/pc/conclaveChallenges Example Response}
  module ConclaveChallenges
    # Gets the current conclave challenges.
    # @return [Array<Warframe::Models::ConclaveChallenge>]
    def conclave_challenges
      get('/conclaveChallenges', Warframe::Models::ConclaveChallenge)
    end
  end
end
