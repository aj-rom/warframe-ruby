# frozen_string_literal: true

require_rel '../models/cetus'

module Warframe::API
  # API endpoint for getting information on current Cetus data.
  #
  # {https://api.warframestat.us/pc/cetusCycle Example Response}
  module Cetus
    # Gets the current cetusCycle Data.
    # @return [Warframe::Models::Cetus]
    def cetus
      get('/cetusCycle', Warframe::Models::Cetus)
    end
  end
end
