# frozen_string_literal: true

require_rel '../models/news'

module Warframe::API
  # API endpoint for getting information from the News route.
  #
  # {https://api.warframestat.us/pc/news Example Response}
  module News
    # Gets the current news data.
    # @return [Array<Warframe::Models::News>]
    def news
      get('/news', Warframe::Models::News)
    end
  end
end
