# frozen_string_literal: true

require_rel '../models/sortie'

module Warframe::API
  # API endpoint for getting information on current Sortie data.
  #
  # {https://api.warframestat.us/pc/sortie Example Response}
  module Sortie
    # Gets the current sortie missions.
    # @return [Array<Warframe::Models::Sortie>]
    def sortie
      get('/sortie', Warframe::Models::Sortie)
    end
  end
end
