# frozen_string_literal: true

require_rel '../models/steel_path'

module Warframe::API
  # API endpoint for getting information on current SteelPath data.
  #
  # {https://api.warframestat.us/pc/steelPath Example Response}
  module SteelPath
    # Steel Path data
    # @return [Warframe::Models::SteelPath]
    def steel_path
      get('/steelPath', Warframe::Models::SteelPath)
    end
  end
end
