# frozen_string_literal: true

require_rel '../models/syndicate_mission'

module Warframe::API
  # API endpoint for getting information on current SyndicateMission data.
  #
  # {https://api.warframestat.us/pc/syndicateMissions Example Response}
  module SyndicateMissions
    # Gets the current syndicateMissions Data.
    # @return [Array<Warframe::Models::SyndicateMission>]
    def syndicate_missions
      get('/syndicateMissions', Warframe::Models::SyndicateMission)
    end
  end
end
