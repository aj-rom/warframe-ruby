# frozen_string_literal: true

require_rel '../models/vallis_cycle'

module Warframe::API
  # API endpoint for getting information on current VallisCycle data.
  #
  # {https://api.warframestat.us/pc/vallisCycle Example Response}
  module VallisCycle
    # Gets the current vallisCycle Data.
    # @return [Warframe::Models::VallisCycle]
    def vallis_cycle
      get('/vallisCycle', Warframe::Models::VallisCycle)
    end
  end
end
