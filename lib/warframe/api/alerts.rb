# frozen_string_literal: true

module Warframe::API
  # API endpoint for getting information on current Alerts data.
  #
  # {https://api.warframestat.us/pc/alerts Example Response}
  module Alerts
    # Gets the current Alerts data.
    # @return [Array<[Warframe::Models::Alert]>]
    def alerts
      get('/alerts', Warframe::Models::Alert)
    end
  end
end
