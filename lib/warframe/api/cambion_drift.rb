# frozen_string_literal: true

require_rel '../models/cambion_drift'

module Warframe::API
  # API endpoint for getting information on current Cambion Drift data.
  #
  # {https://api.warframestat.us/pc/cambionDrift Example Response}
  module CambionDrift
    # Gets the current cambionDrift Data.
    # @return Warframe::Models::CambionDrift
    def cambion_drift
      get('/cambionCycle', Warframe::Models::CambionDrift)
    end
  end
end
