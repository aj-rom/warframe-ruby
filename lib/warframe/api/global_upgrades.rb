# frozen_string_literal: true

require_rel '../models/global_upgrade'

module Warframe::API
  # API endpoint for getting information on current Global Upgrades data.
  #
  # {https://api.warframestat.us/pc/globalUpgrades Example Response}
  module GlobalUpgrades
    # Gets the current Global Upgrades data.
    # @return [Array<[Warframe::Models::GlobalUpgrade]>]
    def global_upgrades
      get('/globalUpgrades', Warframe::Models::GlobalUpgrade)
    end
  end
end
