# frozen_string_literal: true

require_rel '../models/nightwave'

module Warframe::API
  # API endpoint for getting information from the Nightwave route.
  #
  # {https://api.warframestat.us/pc/nightwave Example Response}
  module Nightwave
    # Gets the current Nightwave Mission data.
    # @return [Warframe::Models::Nightwave]
    def nightwave
      get('/nightwave', Warframe::Models::Nightwave)
    end
  end
end
