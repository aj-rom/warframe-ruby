# frozen_string_literal: true

require_rel 'api'
require_rel 'cache'

module Warframe
  # The main application Client for access live feed data.
  #
  # == Example Usage
  #
  #   client = Warframe::Client.new
  #   client.nightwave.tag # => 'Radio Legion Intermission'
  #
  # == Accepted Platforms
  #
  #   default = 'pc'
  #   all_platorms = [ 'pc', 'ps4', 'xb1', 'swi' ]
  #
  #   client = Waframe::Client.new(platform: 'ps4')
  #   client.platform # => 'ps4'
  #   client.language # => 'en'
  #
  # == Accepted Languages
  #
  #   default = 'en'
  #   all_languages = [ 'de', 'es', 'en', 'fr', 'it', 'ko', 'pl', 'pt', 'ru', 'zh' ]
  #
  #   client = Warframe::Client.new(language: 'fr')
  #   client.language # => 'fr'
  #   client.platform # => 'pc'
  #
  # == Setting both Platform and Language
  #
  #   client = Warframe::Client.new(platform: 'ps4', language: 'de')
  #   client.platform # => 'ps4'
  #   client.language # => 'de'
  class Client < Cache
    include API

    # The base Warframe Stat API link
    BASE_URL = 'https://api.warframestat.us/'
    # Default attributes
    DEFAULT_OPTIONS = { platform: 'pc', language: 'en' }.freeze

    # The Client Cache
    attr_accessor :platform, :language

    # Initialize the client.
    #
    # @param platform: [String] 'pc', 'ps4', 'xb1', or 'swi'
    # @param language: [String] 'de', 'es', 'en', 'fr', 'it', 'ko', 'pl', 'pt', 'ru', or 'zh'
    #
    # @return [Warframe::Client] The client instance.
    def initialize(platform: 'pc', language: 'en')
      super({})
      @platform = platform
      @language = language
    end

    # @return [String] the base url and platform combined.
    def base_url
      "#{BASE_URL}#{platform}"
    end

    # Performs a get operation on the requested path, and returns a mapped response of the requested model.
    # @param path [String]
    # @param klass [Warframe::Models]
    def get(path, klass)
      return get_from_cache(path) if find_in_cache(path)

      result = get_request(path, klass)
      add_to_cache(path, result)
    end

    private

    def get_request(path, klass)
      url = "#{base_url}#{path}?language=#{language}"
      begin
        response = RestClient.get(url)
      rescue StandardError => e
        raise e
      end

      return_parsed(response.body, klass)
    end

    # Returns the parsed JSON response in the form of a [Warframe::Models] or an array of [Warframe::Models]
    # @param resp [Net::HTTP.get]
    # @return [Warframe::Models, Array<[Warframe::Models]>]
    def return_parsed(resp, klass)
      parsed = JSON.parse(resp)
      return klass.from_array(parsed) if parsed.is_a?(Array)

      klass.new(parsed)
    end
  end
end
