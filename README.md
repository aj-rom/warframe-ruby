# warframe-ruby

[![Supported by Warframe Community Developers](https://warframestat.us/wfcd.png)](https://github.com/WFCD/banner/blob/main/PROJECTS.md "Supported by Warframe Community Developers")
[![Ruby Gem Version](https://badgen.net/rubygems/v/warframe)](https://rubygems.org/gems/warframe)
[![Downloads](https://badgen.net/rubygems/dt/warframe)](https://rubygems.org/gems/warframe)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop/rubocop)
[![Platform](https://badgen.net/rubygems/p/warframe)](https://www.ruby-lang.org/en/)

A Ruby client for [Warframe Stat](https://warframestat.us) bringing you live modeled data from their API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'warframe'
```

And then execute:

```console
bundle install
```

Or install it yourself as:

```console
gem install warframe
```

## Usage

To get started accessing live data, you must instantiate a new `Warframe::Client`

```ruby
require 'warframe'

Warframe::Client.new # => <Warframe::Client platform='pc' language='en'>

Warframe::Client.new(platform: 'xb1', language: 'fr') # => <Warframe::Client platform='xb1' language='fr'>

# You can supply a hash to set your preferred platform or language.
my_custom_options = { platform: 'ps4', language: 'de' }
Warframe::Client.new my_custom_options # => <Warframe::Client platform='ps4' language='de'>
```

This gives us access to a plethora of methods, check out the [documentation](https://rubydoc.info/gems/warframe) for more info.

```ruby
client = Warframe::Client.new
client.nightwave # => <Warframe::Models::Nightwave>
client.alerts # => [ <Warframe::Models::Alert>, <Warframe::Models::Alert> ]
```

## Documentation

Documentation is currently provided by [rubydoc.info](https://rubydoc.info), check out the current versions documentation at <https://rubydoc.info/gems/warframe>.

## Development

### Ruby Versions

We plan to support only support *stable* Ruby versions that have yet to reach [end of life](https://endoflife.date/ruby).
Although leave the required Ruby version to the lowest of our runtime dependencies in case anyone would like to use an older version.

| Version | Tested | Status |
| --- | --- | --- |
| `3.1` | :white_check_mark: | :white_check_mark: |
| `3.0` | :white_check_mark: | :white_check_mark: |
| `2.7` | :white_check_mark: | :white_check_mark: |
| `2.3+` | :no_entry_sign: | :white_check_mark: |
| `2.2-` | :no_entry_sign: | :no_entry_sign: |

### Dependency Management

After checking out the repo, run:

- ```bundle install```

> This will install all required dependencies for this gem to run.

### Testing

To check if your changes are passing tests, run:

- ```bundle exec rspec```

> Testing framework provided by [RSpec](https://rspec.info/).

### Linting

To auto-correct styling offenses, run:

- ```bundle exec rubocop -A```

> All linting is provided by [RuboCop](https://github.com/rubocop/rubocop).

## Contributing

Bug reports and pull requests are welcome on GitLab at <https://gitlab.com/aj-rom/warframe-ruby>. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/aj-rom/warframe-ruby/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
