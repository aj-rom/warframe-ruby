# Changelog

### [1.0.0](https://www.gitlab.com/aj-rom/warframe-ruby/compare/v0.4.0...v1.0.0) (2022-06-1)

#### Breaking Changes

- **dependencies** use require_all to load moudlar project ([72359701](https://gitlab.com/aj-rom/warframe-ruby/-/merge_requests/65/diffs?commit_id=72359701addd0f588757a4a9122e94b275c490f3))
- **repository:** Moved to https://gitlab.com/aj-rom/warframe-ruby which in turn broke some pre-existing lings on older versions.
- **refactor:**
  - remove no longer used helper files ([f312f5f7](https://gitlab.com/aj-rom/warframe-ruby/-/merge_requests/65/diffs?commit_id=fef7852d7a255093fb6e2e7a1209614bd5dfa914))
  - Remodularize 'Rest' into main module ([9d681b4b](https://gitlab.com/aj-rom/warframe-ruby/-/merge_requests/65/diffs?commit_id=9d681b4b91f53e0f06a1d0b62dd851aef56248b1))
  - Remove unecessary client wrapper ([8956cfe6](https://gitlab.com/aj-rom/warframe-ruby/-/merge_requests/65/diffs?commit_id=8956cfe63465594a860d8df1c42fd5c899ab4a88))

#### Features

- **cache:** allow cache to be instantiated from hash ([fef7852](https://gitlab.com/aj-rom/warframe-ruby/commit/fef7852d7a255093fb6e2e7a1209614bd5dfa914))
- **dependencies:** Update runtime dependencies ([65086ca](https://gitlab.com/aj-rom/warframe-ruby/commit/65086ca6795d4d8352adc8926bda9938ffde152a))
- **refactor:** Remodularize 'Models' to include attributes ([e3eae5d](https://gitlab.com/aj-rom/warframe-ruby/commit/e3eae5de6cd4be7ec88643038d3d8d6a9ab49bf4))

#### Bug Fixes

- **Gem:** resolve ruby versions less than 2.5 being unable to install ([b53247c](https://gitlab.com/aj-rom/warframe-ruby/commit/b53247c23524c28a890114208a9ecd6ec22468d2))
- **README:** update README for v1.0.0 ([c623c2a](https://gitlab.com/aj-rom/warframe-ruby/commit/c623c2aa75e73ed1becdbad01a8ce7fa770a4a9e))

### [0.4.0](https://www.gitlab.com/aj-rom/warframe-ruby/compare/v0.3.2...v0.4.0) (2022-01-02)

#### Features

- **REST:** add `vallisCycle` route. ([#61](https://www.gitlab.com/aj-rom/warframe-ruby/issues/61)) ([f81577f](https://www.gitlab.com/aj-rom/warframe-ruby/commit/f81577fc6af0ec45948cfd57cc1d67de684ce03c))

### [0.3.2](https://www.gitlab.com/aj-rom/warframe-ruby/compare/v0.3.1...v0.3.2) (2021-12-29)

#### Bug Fixes

- **changelog:** added changelog to gemspec. [#54](https://www.gitlab.com/aj-rom/warframe-ruby/issues/54) ([0164546](https://www.gitlab.com/aj-rom/warframe-ruby/commit/01645466c75069800b2f69f1146625e5dc6f6775))
- **dependencies:** add support for no bundler ([741aff8](https://www.gitlab.com/aj-rom/warframe-ruby/commit/741aff8c9d271c80419d1c8046aab37889443514))
- **dependencies:** remove lock file ([86890fe](https://www.gitlab.com/aj-rom/warframe-ruby/commit/86890fec76e702db5d8ee397d875aae28299a262))
- **docs:** fix incorrect attribute module ([cad978b](https://www.gitlab.com/aj-rom/warframe-ruby/commit/cad978bc42679e6a7240b5f6045b70e0fc157f3c))

### [0.3.1](https://www.gitlab.com/aj-rom/warframe-ruby/compare/v0.3.0...v0.3.1) (2021-12-14)

#### Bug Fixes

- **dependencies:** generify runtime bundler dependency ([d0bd8ad](https://www.gitlab.com/aj-rom/warframe-ruby/commit/d0bd8ad2f4096dade9175461c0c4a1c8cc2114ca))
- **dependencies:** update thor to v1.1.0 ([2598318](https://www.gitlab.com/aj-rom/warframe-ruby/commit/2598318b351cfd07f7a691305cc1141681db598b))
