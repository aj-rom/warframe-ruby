# frozen_string_literal: true

require 'require_all'
require 'webmock/rspec'
require_rel 'helpers'
require_rel 'simplecov_profile'

require 'warframe'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include Helpers
end

RSpec::Matchers.define :be_a_boolean do
  match do |real|
    expect(real).to be(true).or be(false)
  end
end
