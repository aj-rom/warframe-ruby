# frozen_string_literal: true

RSpec.describe Warframe::Models::Alert do
  let(:json) { load_fixture 'alerts' }
  let(:alert) { described_class.new json.first }

  it 'can be instantiated from JSON' do
    expect { alert }.not_to raise_error
  end

  it 'can be instantiated from JSON Array' do
    expect { described_class.from_array json }.not_to raise_error
  end

  it 'can read the newly created objects attributes' do
    expect(alert.mission.node).to eq 'Helene (Saturn)'
  end
end
