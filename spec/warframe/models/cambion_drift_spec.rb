# frozen_string_literal: true

RSpec.describe Warframe::Models::CambionDrift do
  let(:json) { load_fixture 'cambion_drift' }
  let(:drift) { described_class.new json }

  it 'can be instantiated from JSON' do
    expect { drift }.not_to raise_error
  end

  it 'can read the newly created objects attributes' do
    expect(drift.active).to be_a String
  end
end
