# frozen_string_literal: true

RSpec.describe Warframe::Models::SteelPath do
  let(:json) { load_fixture 'steel_path' }
  let(:steel_path) { described_class.new(json) }

  it 'can be instantiated from JSON' do
    expect { described_class.new(json) }.not_to raise_error
  end

  it 'can read Steel Path data' do
    expect(steel_path.current_reward.name).to eq '30,000 Endo'
  end
end
