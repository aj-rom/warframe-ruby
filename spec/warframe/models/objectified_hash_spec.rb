# frozen_string_literal: true

RSpec.describe Warframe::Models::ObjectifiedHash do
  let(:json) { load_fixture 'example' }

  it 'can be instantiated from JSON' do
    expect { described_class.new json }.not_to raise_error
  end

  it 'can be created from a JSON Array' do
    array = Array.new(6, json)
    expect { described_class.from_array array }.not_to raise_error
  end

  context 'when instantiated' do
    subject(:dc) { described_class.new json }

    describe 'can access by' do
      it 'method reference' do
        expect(dc.string).to eq 'test'
      end

      it 'hash symbolic reference' do
        expect(dc[:string]).to eq 'test'
      end

      it 'hash string reference' do
        expect(dc['string']).to eq 'test'
      end
    end

    describe 'access data correctly for' do
      it 'arrays' do
        pp dc.inspect
        expect(dc.array).to be_a(Array)
      end
    end
  end
end
