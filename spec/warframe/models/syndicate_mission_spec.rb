# frozen_string_literal: true

RSpec.describe Warframe::Models::SyndicateMission do
  let(:json) { load_fixture 'syndicate_missions' }
  let(:mission) { described_class.new(json[0]) }

  it 'can be instantiated from JSON' do
    expect { described_class.new(json[0]) }.not_to raise_error
  end

  it 'can read SyndicateMission data' do
    expect(mission.syndicate).to eq 'Ostrons'
  end
end
