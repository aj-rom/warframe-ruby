# frozen_string_literal: true

RSpec.describe Warframe::Models::Cetus do
  let(:json) { load_fixture 'cetus' }
  let(:cetus) { described_class.new json }

  it 'can be instantiated from JSON' do
    expect { cetus }.not_to raise_error
  end

  it 'can read the newly created objects attributes' do
    expect(cetus.day?).to be_a_boolean
  end
end
