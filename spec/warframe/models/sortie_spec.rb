# frozen_string_literal: true

RSpec.describe Warframe::Models::Sortie do
  let(:json) { load_fixture 'sortie' }
  let(:sortie) { described_class.new(json) }

  it 'can be instantiated from JSON' do
    expect { described_class.new(json) }.not_to raise_error
  end

  it 'can read Sortie data' do
    expect(sortie.reward_pool).to eq 'Sortie Rewards'
  end
end
