# frozen_string_literal: true

RSpec.describe Warframe::Models::News do
  let(:json) { load_fixture 'news' }
  let(:news) { described_class.from_array json }
  let(:ic) { news.first }

  it 'can be instantiated with JSON input' do
    expect { described_class.new(json.first) }.not_to raise_error
  end

  it 'can be instantiated from a JSON array' do
    expect { described_class.from_array(json) }.not_to raise_error
  end

  it 'can access instance variables' do
    expect(ic.id).not_to be_nil
  end

  it 'can access boolean that end with ?' do
    expect(ic.prime_access?).not_to be_nil
  end
end
