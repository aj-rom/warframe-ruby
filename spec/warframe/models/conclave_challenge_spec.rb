# frozen_string_literal: true

RSpec.describe Warframe::Models::ConclaveChallenge do
  let(:json) { load_fixture 'conclave_challenges' }
  let(:challenge) { described_class.new(json.first) }

  it 'can be instantiated from JSON' do
    expect { challenge }.not_to raise_error
  end

  it 'can be instantiated from JSON Array' do
    expect { described_class.from_array(json) }.not_to raise_error
  end

  it 'can read Conclave Challenge data' do
    expect(challenge.mode).to eq 'Any Mode'
  end
end
