# frozen_string_literal: true

RSpec.describe Warframe::Models::VallisCycle do
  let(:json) { load_fixture 'vallis_cycle' }
  let(:cycle) { described_class.new(json) }

  it 'can be instantiated from JSON' do
    expect { described_class.new(json) }.not_to raise_error
  end

  it 'can read VallisCycle data' do
    expect(cycle.state).to eq 'cold'
  end
end
