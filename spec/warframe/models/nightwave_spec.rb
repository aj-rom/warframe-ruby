# frozen_string_literal: true

RSpec.describe Warframe::Models::Nightwave do
  let(:json) { load_fixture 'nightwave' }
  let(:model) { described_class.new json }

  it 'can be instantiated from JSON' do
    expect { described_class.new json }.not_to raise_error
  end

  it 'can access instance variables' do
    expect(model.id).not_to be_nil
  end

  it 'can access nested attributes' do
    expect(model.possible_challenges).to be_a Array
  end

  it 'maintains Hash structure of nested attributes' do
    expect(model.possible_challenges[0].id).not_to be_nil
  end
end
