# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::VallisCycle do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('vallis_cycle') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#vallis_cycle' do
    it 'does not raise error on call' do
      expect { client.vallis_cycle }.not_to raise_error
    end

    it 'properly loads data into model' do
      expect(client.vallis_cycle).to be_a Warframe::Models::VallisCycle
    end
  end
end
