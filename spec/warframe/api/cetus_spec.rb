# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::Cetus do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('cetus') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#cetus' do
    it 'does not raise error on call' do
      expect { client.cetus }.not_to raise_error
    end

    it 'properly loads data into model' do
      expect(client.cetus).to be_a Warframe::Models::Cetus
    end
  end
end
