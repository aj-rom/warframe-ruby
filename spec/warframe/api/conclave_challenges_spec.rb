# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::ConclaveChallenges do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('conclave_challenges') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#conclave_challenges' do
    it 'does not raise error on call' do
      expect { client.conclave_challenges }.not_to raise_error
    end

    it 'returns an array' do
      expect(client.conclave_challenges).to be_a Array
    end

    it 'properly loads the data into a model' do
      expect(client.conclave_challenges.first).to be_a Warframe::Models::ConclaveChallenge
    end
  end
end
