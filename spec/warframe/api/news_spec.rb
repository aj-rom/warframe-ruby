# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::News do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('news') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#news' do
    it 'does not raise error on call' do
      expect { client.news }.not_to raise_error
    end

    it 'returns an array' do
      expect(client.news).to be_a Array
    end

    it 'properly loads the data into a model' do
      expect(client.news.first).to be_a Warframe::Models::News
    end
  end
end
