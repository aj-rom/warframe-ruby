# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::Nightwave do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('nightwave') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#nightwave' do
    it 'does not raise error on call' do
      expect { client.nightwave }.not_to raise_error
    end

    it 'properly loads data into model' do
      expect(client.nightwave).to be_a Warframe::Models::Nightwave
    end
  end
end
