# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::Sortie do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('sortie') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#sortie' do
    it 'does not raise error on call' do
      expect { client.sortie }.not_to raise_error
    end

    it 'properly loads the data into a model' do
      expect(client.sortie).to be_a Warframe::Models::Sortie
    end
  end
end
