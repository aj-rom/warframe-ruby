# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::SyndicateMissions do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('syndicate_missions') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#syndicate_missions' do
    it 'does not raise error on call' do
      expect { client.syndicate_missions }.not_to raise_error
    end

    it 'returns an array' do
      expect(client.syndicate_missions).to be_a Array
    end

    it 'properly loads the data into a model' do
      expect(client.syndicate_missions.first).to be_a Warframe::Models::SyndicateMission
    end
  end
end
