# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::CambionDrift do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('cambion_drift') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#cambion_drift' do
    it 'does not raise error on call' do
      expect { client.cambion_drift }.not_to raise_error
    end

    it 'properly loads data into model' do
      expect(client.cambion_drift).to be_a Warframe::Models::CambionDrift
    end
  end
end
