# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::SteelPath do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('steel_path') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#steel_path' do
    it 'does not raise error on call' do
      expect { client.steel_path }.not_to raise_error
    end

    it 'properly loads the data into a model' do
      expect(client.steel_path).to be_a Warframe::Models::SteelPath
    end
  end
end
