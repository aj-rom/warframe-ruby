# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::Alerts do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('alerts') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
    allow(JSON).to receive(:parse).and_call_original
  end

  describe '#alerts' do
    it 'does not raise error on call' do
      expect { client.alerts }.not_to raise_error
    end

    it 'returns an array of alerts' do
      expect(client.alerts).to be_a Array
    end

    it 'properly loads data into model' do
      expect(client.alerts.first).to be_a(Warframe::Models::Alert)
    end
  end
end
