# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::GlobalUpgrades do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('global_upgrades') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#global_upgrades' do
    it 'does not raise error on call' do
      expect { client.global_upgrades }.not_to raise_error
    end

    it 'returns an array' do
      expect(client.global_upgrades).to be_a Array
    end

    it 'properly loads the data into a model' do
      expect(client.global_upgrades.first).to be_a Warframe::Models::GlobalUpgrade
    end
  end
end
