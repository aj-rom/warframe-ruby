# frozen_string_literal: true

# frozen_literal_string: true

RSpec.describe Warframe::API::Invasions do
  let(:client) { Warframe::Client.new }
  let(:response) { json_response('invasions') }

  before do
    allow(RestClient).to receive(:get).and_return(response)
  end

  describe '#invasions' do
    it 'does not raise error on call' do
      expect { client.invasions }.not_to raise_error
    end

    it 'returns an array' do
      expect(client.invasions).to be_a Array
    end

    it 'properly loads the data into a model' do
      expect(client.invasions.first).to be_a Warframe::Models::Invasion
    end
  end
end
