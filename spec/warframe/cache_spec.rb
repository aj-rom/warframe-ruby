# frozen_string_literal: true

RSpec.describe Warframe::Cache do
  subject(:dc) { described_class }

  let(:ic) { dc.new }
  # let(:client) { Warframe::Client.new }
  # let(:old) { Warframe::Models::Nightwave.new(load_fixture('nightwave')) }

  it 'can be instantiated' do
    expect { dc.new }.not_to raise_error
  end

  describe '#get_from_cache' do
    it 'can access the cache' do
      client = dc.new({ '/testing' => { result: true } })
      expect(client.get_from_cache('/testing')).to be_truthy
    end

    it 'returns nil if it does not exist' do
      expect(ic.get_from_cache('n/a')).to be_nil
    end
  end

  describe '#find_in_cache' do
    it 'returns nil when the key doesnt exist' do
      expect(ic.find_in_cache('n/a')).to be_nil
    end
  end

  describe '#add_to_cache' do
    it 'handles adding the data' do
      ic.add_to_cache('/testing', true)
      expect(ic.get_from_cache('/testing')).to be_truthy
    end

    describe 'handles expiration' do
      let(:time) { '1653029102' }
      let(:expired_at) { '1653029402' }

      before do
        allow(Time).to receive(:now).and_return(time, expired_at)
      end

      it 'adds the expiration time' do
        ic.add_to_cache('/testing', true)
        expect(ic.cache['/testing'][:time]).to eq(expired_at.to_i)
      end

      it 'can enter different entries per request' do
        ic.add_to_cache('first', true)
        ic.add_to_cache('second', true)
        expect(ic.cache['first'][:time]).not_to eq(ic.cache['second'][:time])
      end
    end
  end
end
