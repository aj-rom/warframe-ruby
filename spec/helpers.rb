# frozen_string_literal: true

module Helpers
  def load_fixture(file_name, parse: true)
    joined = File.join(__dir__, "fixtures/#{file_name}.json")
    file = File.read(joined)
    file = JSON.parse(file) if parse
    file
  end

  def json_response(file_name)
    data = load_fixture(file_name, parse: false)
    instance_double(RestClient::Response, body: data)
  end
end
